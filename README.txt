Custom Captcha
Built by Sam Elayyoub 

This module is a simple captcha, no need to activate or to worry about Apis. Custom captcha built to make developer's life easier to prevent spam registration and forms,

Features:
1- Create your own list or keywords
2- No JS needed for better performance
3- Secure for harder spam figures

How to use:
1- Enable the module
2- go to the module admin page
3- create your list of keywords, can be words, sentence or numbers
4- check enable box to make it available all around the site for all forms
5- done!
