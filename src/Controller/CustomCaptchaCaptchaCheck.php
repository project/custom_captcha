<?php

namespace Drupal\custom_captcha\Controller;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Controller\ControllerBase;

/**
 * Custom captcha clear class.
 *
 * @CustomCaptchaCaptchaCheck
 * Defines HelloController class.
 */
class CustomCaptchaCaptchaCheck extends ControllerBase {
  /**
   * To check if this variable exist in @getKeyword()
   *
   * @var captchaKey
   */
  protected $captchaKey;

  /**
   * Constructs a new HomeController object.
   */
  public function __construct() {
    $this->captchaKey = 'no';
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * Ajax callback to validate the captcha field.
   */
  public function customCaptchaCalidator($form, $form_state) {
    if ($form_state->hasValue('input_c')) {
      // ($form_state->get('hidden_c'))?:$form_state->getValue('hidden_c_f');
      $captchaCheckId = $form_state->getValue('hidden_c_f');
      // Pass a key => value to other submit callbacks.
      $captchaCheck = $form_state->getValue('input_c');
      $this->captchaKey = $captchaCheckId;
      if (str_replace(' ', '', $captchaCheckId) == str_replace(' ', '', $captchaCheck)) {
        return TRUE;
      }
      else {
        return FALSE;
      }
    }
  }

  /**
   * Custom Captcha @getKeyword.
   */
  public function getKeyword() {
    $keywords = $this->config('custom_captcha.settings')->get('keywords');
    $words = explode("\n", $keywords);
    // Keywords.
    $k = array_rand($words);
    $kyeIs = $words[$k];
    $this->captchaKey = $kyeIs;
    if ($keywords == '') {
      $kyeIs = $this->incrementalHash();
    }
    return $kyeIs;
  }

  /**
   * Random Number @incrementalHash()
   *
   * @var length
   */
  private function incrementalHash($len = 5) {
    $char = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charLength = strlen($char);
    $random = '';
    for ($i = 0; $i < $len; $i++) {
      $random .= $char[rand(0, $charLength - 1)];
    }
    return $random;
  }

  /**
   * Load captcha @customCaptchaAjaxCallbackLoad.
   *
   * @var form
   * @var form_state
   * @var form_id
   */
  public function customCaptchaAjaxCallbackLoad(array &$form, FormStateInterface $form_state, $form_id) {
    // Generate captcha if not there.
    $kyeIs = $this->getKeyword();
    $nowWordsIs = $kyeIs;
    $form_state->setFormState([
      'hidden_c' => $nowWordsIs,
    ]);
    return $form_state;
  }

}
