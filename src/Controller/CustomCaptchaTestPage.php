<?php

namespace Drupal\custom_captcha\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Defines HelloController class.
 */
class CustomCaptchaTestPage extends ControllerBase {

  /**
   * Display the markup.
   *
   * @return array
   *   Return markup array.
   */
  public function content() {
    $extra_content_message = $this->config('custom_captcha.settings')->get('keywords');
    $body = $extra_content_message;

    return [
      '#type' => 'markup',
      '#markup' => 'Keywords are:' . $body,
    ];
  }

}
