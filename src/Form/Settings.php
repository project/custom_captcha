<?php

namespace Drupal\custom_captcha\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure settings for this site.
 */
class Settings extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'custom_captcha_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'custom_captcha.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('custom_captcha.settings');
    $form['enabled_check'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Custom Captcha.'),
      '#description' => $this->t('Enable/Disable notification on user registration'),
      '#default_value' => $config->get('enabled_check'),
    ];
    $form['show_form_id'] = [
      '#type' => 'checkbox',
      '#title' => $this->t("Show form's ID on each form."),
      '#default_value' => $config->get('show_form_id'),
    ];
    $form['hide_submit'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Hide submit in from the form till captcha validation done, on fail hide it again.'),
      '#default_value' => $config->get('hide_submit'),
    ];
    $form['keywords'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Keywords'),
      '#description' => $this->t('Leave it null for random words, Keywords can be simple words, sentences, or even numbers. Each word/sentence/number should be in a separate line, example:<br>keys<br> life is amazing<br>I love this module.'),
      '#default_value' => $config->get('keywords'),
    ];

    // Create vertical tab.
    $form['form_option'] = [
      '#type' => 'details',
      '#title' => 'Forms to add Captcha to',
      '#group' => 'additional_settings',
    ];
    $form['form_option']['contact_us_form'] = [
      '#type' => 'checkbox',
      '#title' => 'Contact us form',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#weight' => 10,
      '#default_value' => $config->get('contact_us_form'),
    ];
    $form['form_option']['user_register_form'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('User register form'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#weight' => 10,
      '#default_value' => $config->get('user_register_form'),
    ];
    $form['form_option']['other_form_ids'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Other Forms'),
      '#collapsible' => TRUE,
      '#description' => $this->t('Example: search_block_form, contact_message_feedback_form, ...'),
      '#collapsed' => TRUE,
      '#weight' => 10,
      '#default_value' => $config->get('other_form_ids'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration.
    $this->configFactory->getEditable('custom_captcha.settings')
        // Set the submitted configuration setting.
      ->set('keywords', $form_state->getValue('keywords'))
      ->set('enabled_check', $form_state->getValue('enabled_check'))
      ->set('form_option', $form_state->getValue('form_option'))
      ->set('contact_us_form', $form_state->getValue('contact_us_form'))
      ->set('user_register_form', $form_state->getValue('user_register_form'))
      ->set('hide_submit', $form_state->getValue('hide_submit'))
      ->set('other_form_ids', $form_state->getValue('other_form_ids'))
      ->set('show_form_id', $form_state->getValue('show_form_id'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
